package profi.android.testapp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import profi.android.testapp.R;
import profi.android.testapp.backend.entity.VKUser;
import profi.android.testapp.backend.request.FriendsRequest;
import profi.android.testapp.backend.request.RequestListener;
import profi.android.testapp.backend.service.RequestServiceHelper;


public class FriendsListActivity extends AppCompatActivity implements RequestListener {
    private RequestServiceHelper requestServiceHelper;
    private RecyclerView friendsList;
    private ProgressBar progressBar;

    final static String USER_FRIENDS_ID = "125930227";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        friendsList = (RecyclerView)findViewById(R.id.friends_list);
        progressBar = (ProgressBar)findViewById(R.id.progress);

        friendsList.setHasFixedSize(true);
        friendsList.setLayoutManager(new LinearLayoutManager(this));

        requestServiceHelper = new RequestServiceHelper(getApplication());
        requestServiceHelper.onStart();

        requestServiceHelper.startRequest(new FriendsRequest(USER_FRIENDS_ID), this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestServiceHelper.onStop();
    }

    private void onLoadComplete() {
        this.progressBar.setVisibility(View.GONE);
        this.friendsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(Bundle bundle) {
        ArrayList<VKUser> users = bundle.getParcelableArrayList("users");
        friendsList.setAdapter(new FriendsListAdapter(this, users));

        Toast.makeText(this, "Loaded " + users.size() + " friends", Toast.LENGTH_LONG).show();
        onLoadComplete();
    }

    @Override
    public void onFailure(Bundle bundle) {
        Toast.makeText(this, "onFailure", Toast.LENGTH_LONG).show();
        onLoadComplete();
    }
}
