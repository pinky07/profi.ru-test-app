package profi.android.testapp.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import profi.android.testapp.R;
import profi.android.testapp.backend.entity.VKUser;
import profi.android.testapp.backend.request.RequestListener;
import profi.android.testapp.backend.request.UserRequest;
import profi.android.testapp.backend.service.RequestServiceHelper;
import profi.android.testapp.util.FetchImageTask;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 4:21
 */
public class UserPhotoActivity extends AppCompatActivity implements RequestListener, View.OnClickListener {
    private static final String EXTRA_IMAGE = "extra_image";
    private static final String EXTRA_USER_ID = "extra_user_id";
    private static final String EXTRA_IMAGE_SRC = "extra_image_src";
    private RequestServiceHelper requestServiceHelper;
    private ImageView photoImage;

    public static void navigate(AppCompatActivity activity, View transitionImage, String userId, String imageSrc) {
        Intent intent = new Intent(activity, UserPhotoActivity.class);
        intent.putExtra(EXTRA_USER_ID, userId);
        intent.putExtra(EXTRA_IMAGE_SRC, imageSrc);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_IMAGE);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photo);
        photoImage = (ImageView)findViewById(R.id.user_photo);
        photoImage.setOnClickListener(this);

        ViewCompat.setTransitionName(photoImage, EXTRA_IMAGE);
        supportPostponeEnterTransition();

        requestServiceHelper = new RequestServiceHelper(getApplication());
        requestServiceHelper.onStart();
        new FetchImageTask(photoImage) {
            @Override
            protected void onPreExecute() {}
            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);
                supportStartPostponedEnterTransition();

                requestServiceHelper.startRequest(new UserRequest(getIntent().getStringExtra(EXTRA_USER_ID)),
                        UserPhotoActivity.this);
            }
        }.execute(getIntent().getStringExtra(EXTRA_IMAGE_SRC));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestServiceHelper.onStop();
    }

    @Override
    public void onSuccess(Bundle bundle) {
        VKUser user = bundle.getParcelable("user");
        new FetchImageTask(photoImage) {
            @Override
            protected void onPreExecute() {}

            @Override
            protected Bitmap doInBackground(String... urls) {
                try {
                    Thread.sleep(1000);
                } catch (Exception ex) {
                    //pass
                }

                return super.doInBackground(urls);
            }
        }.execute(user.getPhotoBig());

        Toast.makeText(this, "Loaded big image", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(Bundle bundle) {
        Toast.makeText(this, "Load big image failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_photo:
                supportFinishAfterTransition(); break;
        }
    }
}
