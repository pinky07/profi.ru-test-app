package profi.android.testapp.ui;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import profi.android.testapp.R;
import profi.android.testapp.backend.entity.VKUser;
import profi.android.testapp.util.FetchImageTask;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 2:38
 */
public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder> {
    List<VKUser> users;
    RecyclerView recyclerView;
    AppCompatActivity activity;

    public FriendsListAdapter(AppCompatActivity activity, List<VKUser> users) {
        this.activity = activity;
        this.users = users;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView userFio;
        public ImageView avatarImageView;
        public View parentView;
        public ViewHolder(View viewGroup) {
            super(viewGroup);
            parentView = viewGroup;
            avatarImageView = (ImageView)viewGroup.findViewById(R.id.user_avatar);
            userFio= (TextView)viewGroup.findViewById(R.id.user_fio);
        }

        public void onItemClickListener(View.OnClickListener onClickListener) {
            parentView.setOnClickListener(onClickListener);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_friends_list, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final VKUser vkUser = users.get(i);
        viewHolder.userFio.setText(String.format("%s %s", vkUser.getFirstName(), vkUser.getLastName()));
        new FetchImageTask(viewHolder.avatarImageView).execute(vkUser.getPhoto());

        viewHolder.onItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserPhotoActivity.navigate(activity, viewHolder.avatarImageView, vkUser.getUserId(), vkUser.getPhoto());
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
