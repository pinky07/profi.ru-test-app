package profi.android.testapp.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 2:36
 * Класс загрузчик изображений
 */
public class FetchImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    static LruCache<String, Bitmap> cache = new LruCache<>(4 * 1024 * 1024); // 4 мб

    public FetchImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.bmImage.setImageBitmap(null);
    }

    protected Bitmap doInBackground(String... urls) {
        String imageUrl = urls[0];
        Bitmap bitmap = null;

        //если есть в кэше, то достаем оттуда
        if((bitmap = cache.get(imageUrl)) != null) {
            return bitmap;
        }

        try {
            InputStream in = new java.net.URL(imageUrl)
                    .openStream();
            bitmap = BitmapFactory.decodeStream(in);

            //сохраняем в кэш
            cache.put(imageUrl, bitmap);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}
