package profi.android.testapp.backend.network;

import java.util.List;

import profi.android.testapp.backend.entity.VKUser;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 1:09
 */
public interface VKClient {
    /**
     * Получение списка друзей пользователя
     * @param userId
     * @return
     */
    public List<VKUser> getFriends(String userId);

    /**
     * Получение информации о пользователе
     * @param userId
     * @return
     */
    public VKUser getUser(String userId);
}
