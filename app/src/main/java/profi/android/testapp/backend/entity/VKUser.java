package profi.android.testapp.backend.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 25.07.15
 * Time: 16:29
 */
public class VKUser implements Parcelable {
    private String userId;
    private String firstName;
    private String lastName;
    private String photo;
    private String photoBig;

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhotoBig() {
        return photoBig;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setPhotoBig(String photoBig) {
        this.photoBig = photoBig;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.photo);
        dest.writeString(this.photoBig);
    }

    public VKUser() {
    }

    private VKUser(Parcel in) {
        this.userId = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.photo = in.readString();
        this.photoBig = in.readString();
    }

    public static final Creator<VKUser> CREATOR = new Creator<VKUser>() {
        public VKUser createFromParcel(Parcel source) {
            return new VKUser(source);
        }

        public VKUser[] newArray(int size) {
            return new VKUser[size];
        }
    };
}
