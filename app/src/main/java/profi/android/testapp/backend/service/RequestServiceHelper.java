package profi.android.testapp.backend.service;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.SparseArray;

import java.util.concurrent.atomic.AtomicInteger;

import profi.android.testapp.backend.request.BaseRequest;
import profi.android.testapp.backend.request.RequestListener;

/**
 * Author: Khamidullin Kamil
 * Date: 25.07.15
 * Time: 15:32
 */
public class RequestServiceHelper {
    private SparseArray<RequestListener> currentListeners = new SparseArray<RequestListener>();
    private AtomicInteger idCounter = new AtomicInteger();

    private Application application;

    private boolean isUIActive = false;


    public RequestServiceHelper(Application application) {
        this.application = application;
    }

    public void onStart() {
        this.isUIActive = true;
    }

    public void onStop() {
        this.isUIActive = false;
    }

    /**
     * Создает уникальный идентификатор запроса,
     * в дальнейшем можно использовать для отмены запроса
     * @return
     */
    private int createRequestId() {
        return idCounter.getAndIncrement();
    }

    public void cancelRequest(int requestId) {
        application.startService(createCancelRequestIntent(application, requestId));
        currentListeners.remove(requestId);
    }

    public int startRequest(BaseRequest baseRequest, RequestListener requestListener) {
        final int requestId = createRequestId();
        currentListeners.append(requestId, requestListener);
        application.startService(createStartRequestIntent(application, baseRequest, requestId));

        return requestId;
    }

    public boolean isPending(int requestId) {
        return currentListeners.get(requestId) != null;
    }

    /**
     * Формирует интент для отмены запроса на сервисе
     * @param context
     * @param requestId
     * @return
     */
    private Intent createCancelRequestIntent(final Context context, final int requestId) {
        Intent intent = new Intent(context, RequestService.class);
        intent.setAction(RequestService.ACTION_CANCEL);
        intent.putExtra(RequestService.EXTRA_REQUEST_ID, requestId);

        return intent;
    }

    /**
     * Формирует интент для запуска запроса на сервисе
     * @param context
     * @param request
     * @param requestId
     * @return
     */
    private Intent createStartRequestIntent(final Context context, BaseRequest request, final int requestId) {
        Intent intent = new Intent(context, RequestService.class);
        intent.setAction(RequestService.ACTION_START);

        intent.putExtra(RequestService.EXTRA_REQUEST, request);
        intent.putExtra(RequestService.EXTRA_REQUEST_ID, requestId);
        intent.putExtra(RequestService.EXTRA_STATUS_RECEIVER, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (isPending(requestId) && isUIActive) {
                    RequestListener currentListener = currentListeners.get(requestId);
                    if (currentListener != null) {
                        switch (resultCode) {
                            case BaseRequest.RESPONSE_SUCCESS:
                                currentListener.onSuccess(resultData); break;
                            case BaseRequest.RESPONSE_FAILURE:
                                currentListener.onFailure(resultData); break;
                        }

                        currentListeners.remove(requestId);
                    }
                }
            }
        });

        return intent;
    }
}
