package profi.android.testapp.backend.request;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import java.io.Serializable;

import profi.android.testapp.backend.network.VKClient;

/**
 * Author: Khamidullin Kamil
 * Date: 25.07.15
 * Time: 15:10
 * Базовый класс запроса, каждый класс - запрос на сервис должен наследовать его
 */
abstract public class BaseRequest implements Serializable {
    public static final int RESPONSE_SUCCESS = 0;
    public static final int RESPONSE_FAILURE = 1;

    private ResultReceiver resultReceiver;
    protected volatile boolean cancelled = false;

    public final void execute(VKClient vkClient, ResultReceiver resultReceiver) {
        this.resultReceiver = resultReceiver;
        try {
            notifySuccess(doExecute(vkClient));
        } catch (Exception exception) {
            notifyFailure(exception);
        }
    }

    protected abstract Bundle doExecute(VKClient vkClient);

    protected void notifySuccess(Bundle data) {
        sendUpdate(RESPONSE_SUCCESS, data);
    }

    protected void notifyFailure(Exception exception) {
        Bundle data = new Bundle();
        data.putSerializable("exception", exception);
        sendUpdate(RESPONSE_FAILURE, data);
    }

    private void sendUpdate(int resultCode, Bundle data) {
        if (resultReceiver != null) {
            resultReceiver.send(resultCode, data);
        }
    }

    public synchronized void cancel() {
        cancelled = true;
    }
}
