package profi.android.testapp.backend.request;

import android.os.Bundle;

import profi.android.testapp.backend.entity.VKUser;
import profi.android.testapp.backend.network.VKClient;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 12:42
 */
public class UserRequest extends BaseRequest {
    private String userId;

    public UserRequest(String userId) {
        this.userId = userId;
    }

    @Override
    protected Bundle doExecute(VKClient vkClient) {
        VKUser user = vkClient.getUser(userId);

        Bundle data = new Bundle();
        data.putParcelable("user", user);

        return data;
    }
}
