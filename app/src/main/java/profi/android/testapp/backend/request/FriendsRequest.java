package profi.android.testapp.backend.request;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import profi.android.testapp.backend.entity.VKUser;
import profi.android.testapp.backend.network.VKClient;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 2:06
 */
public class FriendsRequest extends BaseRequest {
    private String userId;

    public FriendsRequest(String userId) {
        this.userId = userId;
    }

    @Override
    protected Bundle doExecute(VKClient vkClient) {
        List<VKUser> users = vkClient.getFriends(userId);
        Bundle result = new Bundle();
        result.putParcelableArrayList("users", new ArrayList<VKUser>(users));

        return result;
    }
}
