package profi.android.testapp.backend.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import android.os.Process;
import android.os.ResultReceiver;
import android.util.SparseArray;

import profi.android.testapp.backend.network.VKClient;
import profi.android.testapp.backend.network.VKClientImpl;
import profi.android.testapp.backend.request.BaseRequest;

/**
 * Author: Khamidullin Kamil
 * Date: 25.07.15
 * Time: 15:00
 */
public class RequestService extends Service {
    private final static int THREAD_COUNT = 2;

    public final static String EXTRA_REQUEST_ID = "extra_request_id";
    public static final String EXTRA_REQUEST = "extra_request";
    public static final String EXTRA_STATUS_RECEIVER = "extra_receiver";

    public final static String ACTION_START = "command_start";
    public final static String ACTION_CANCEL = "command_cancel";

    private ExecutorService executor = Executors.newFixedThreadPool(THREAD_COUNT);
    private final SparseArray<RunningRequest> runningRequests = new SparseArray<RunningRequest>();
    private VKClient vkClient;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private int getRequestId(Intent intent) {
        return intent.getIntExtra(EXTRA_REQUEST_ID, -1);
    }

    private BaseRequest getRequest(Intent intent) {
        return (BaseRequest)intent.getSerializableExtra(EXTRA_REQUEST);
    }

    private VKClient getVkClient() {
        if(vkClient == null) {
            vkClient = new VKClientImpl();
        }

        return vkClient;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_START.equals(intent.getAction())) {
            RunningRequest runningCommand = new RunningRequest(getVkClient(), intent);

            synchronized (runningRequests) {
                runningRequests.append(getRequestId(intent), runningCommand);
            }

            executor.submit(runningCommand);
        } else if (ACTION_CANCEL.equals(intent.getAction())) {
            RunningRequest runningRequest = runningRequests.get(getRequestId(intent));
            if (runningRequest != null) {
                runningRequest.cancel();
            }
        }

        return START_NOT_STICKY;
    }

    private ResultReceiver getReceiver(Intent intent) {
        return intent.getParcelableExtra(EXTRA_STATUS_RECEIVER);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        executor.shutdownNow();
    }

    private class RunningRequest implements Runnable {
        private Intent intent;
        private VKClient vkClient;
        private BaseRequest request;

        public RunningRequest(VKClient vkClient, Intent intent) {
            this.intent = intent;
            this.vkClient = vkClient;

            request = getRequest(intent);
        }

        public void cancel() {
            request.cancel();
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            request.execute(vkClient, getReceiver(intent));

            shutdown();
        }

        private void shutdown() {
            synchronized (runningRequests) {
                runningRequests.remove(getRequestId(intent));
                if (runningRequests.size() == 0) {
                    stopSelf();
                }
            }
        }

    }
}
