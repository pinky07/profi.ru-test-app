package profi.android.testapp.backend.network;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 1:07
 * // todo impl POST, PUT, DELETE
 */
abstract public class BaseHttpClient {
    // время получения ответа
    private final static int CONNECTION_TIMEOUT = (int)3E4; // 30 sec timeout
    // время ожидания ответа
    private final static int SOCKET_TIMEOUT = (int)3E4; // 30 sec timeout

    /**
     * GET запрос, возвращает строку
     * @param url
     * @return
     */
    protected String getString(String url) {
        try {
            return read(sendRequest(new HttpGet(url)));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * GET запрос
     * @param url
     * @return
     */
    protected InputStream get(String url) {
        try {
            return sendRequest(new HttpGet(url));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private InputStream sendRequest(HttpRequestBase httpRequest) throws IOException {
        HttpClient httpclient   = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);
        HttpResponse response   = httpclient.execute(httpRequest);
        HttpEntity entity       = response.getEntity();

        return entity.getContent();
    }

    private static String read(InputStream instream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(instream));
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        instream.close();

        return sb.toString();
    }
}
