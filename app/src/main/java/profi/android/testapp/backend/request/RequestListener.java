package profi.android.testapp.backend.request;

import android.os.Bundle;

/**
 * Author: Khamidullin Kamil
 * Date: 25.07.15
 * Time: 15:26
 */
public interface RequestListener {
    public void onSuccess(Bundle bundle);

    public void onFailure(Bundle bundle);
}
