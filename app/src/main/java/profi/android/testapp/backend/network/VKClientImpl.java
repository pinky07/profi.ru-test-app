package profi.android.testapp.backend.network;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import profi.android.testapp.backend.entity.VKUser;

/**
 * Author: Khamidullin Kamil
 * Date: 25.07.15
 * Time: 16:32
 */
public class VKClientImpl extends BaseHttpClient implements VKClient {
    private final static String HOST = "https://api.vk.com/";

    private String buildVKUrl(String method) {
        return HOST + method;
    }

    private List<VKUser> readUsersResponse(InputStream in) {
        try {
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginObject();
            if(reader.hasNext()) {
                String name = reader.nextName();
                if(name.equals("response")) {
                    return readUserList(reader);
                }
            }
            reader.endObject();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        throw new RuntimeException("Wrong response format");
    }

    private List<VKUser> readUserList(JsonReader reader) {
        List<VKUser> users = new ArrayList<VKUser>();
        try {
            reader.beginArray();
            while (reader.hasNext()) {
                users.add(readUser(reader));
            }
            reader.endArray();

            return users;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private VKUser readUser(JsonReader reader) {
        VKUser vkUser = new VKUser();

        try {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("uid")) {
                    vkUser.setUserId(String.valueOf(reader.nextInt()));
                } else if (name.equals("first_name")) {
                    vkUser.setFirstName(reader.nextString());
                } else if (name.equals("last_name")) {
                    vkUser.setLastName(reader.nextString());
                } else if (name.equals("photo")) {
                    vkUser.setPhoto(reader.nextString());
                } else if (name.equals("photo_big")) {
                    vkUser.setPhotoBig(reader.nextString());
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();

            return vkUser;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Получение списка друзей пользователя
     * @param userId
     * @return
     */
    @Override
    public List<VKUser> getFriends(String userId) {
        return readUsersResponse(get(buildVKUrl("method/friends.get?user_id=" + userId + "&fields=photo")));
    }

    /**
     * Получение информации о пользователе
     * @param userId
     * @return
     */
    @Override
    public VKUser getUser(String userId) {
        List<VKUser> users = readUsersResponse(get(buildVKUrl("method/getProfiles?uids=" + userId + "&fields=photo_big")));
        if(!users.isEmpty()) {
            return users.get(0);
        }

        return null;
    }
}
